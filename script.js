/*
1) Опишіть, як можна створити новий HTML тег на сторінці.
2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти 
цього параметра.
3) Як можна видалити елемент зі сторінки?


1) За допомогою методу document.createElement(""тег""")
                       elem.cloneNode(boolean) 
                       document.createTextNode(value) - для текстових вузлів

2) Перший параметр вказує місце куди вставляти html відносно елементу в якого цей метод був визваний.
можливі значення першого параметру : 
      - "beforebegin" - перед відкриваючим тегом
      -"afterbegin"   - після відкриваючого тегу 
      -"beforeend"    - перед закриваючим тегом
      -"afterend"     - після закриваючого тегу

3) Використавши метод .remove()
*/

//Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку
/*
Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];
*/

let getElementsList = (array, parent = document.body) => {
    const list = document.createElement("ol")   

    array.forEach(element => {
        const liElement = document.createElement("li")
        liElement.innerText = element
        list.append(liElement)        
    })
    parent.append(list)
}

const arrayTest1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
const arrayTest2 = ["1", "2", "3", "sea", "user", 23]
getElementsList(arrayTest1)

const list = document.querySelector("ol")
getElementsList(arrayTest2, list)
getElementsList(arrayTest2, list.lastChild)


